const mongoose = require('mongoose')
const baseName = 'libtest'
mongoose.connect('mongodb://localhost:27017/' + baseName, { useNewUrlParser: true })

const dbc = mongoose.connection

dbc.once('open', async () => {
  console.log(`Mongoose Connected [${baseName}]`)
})
