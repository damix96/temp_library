const mongoose = require('mongoose');
mongoose.Promise = global.Promise

const Schema = mongoose.Schema;

const Data = new Schema({
  data: { type: Object }
});

module.exports = mongoose.model('Data', Data);
