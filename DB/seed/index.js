const requireDir = require('require-dir')
const models = requireDir('../models')
const data = requireDir('./data')

const create = async (data, Model) => {
  const promises = data.map(item => new Model(item).save())
  const result = await Promise.all(promises)
  return result
}

const drop = async (query = {}, Model) => new Promise((resolve, reject) => {
  console.warn(`Removing some data in model "${Model.modelName}" with query:`, query)
  console.warn('U have 5 seconds to cancel it')
  setTimeout(async () => {
    const removed = await Model.remove(query)
    console.warn({ removed })
    resolve(removed)
  }, 5000)
})
//
// const init = async () => {
//   // const adm = await create(admins, Admins)
//   // console.log(adm.length, 'Admins created!')
//   await drop({}, Privileges)
//   const privs = await create(privileges, Privileges)
//   console.log(privs.length, 'Privileges created!')
// }
//
// init()
module.exports = {
  create,
  drop,
  models,
  data
}
