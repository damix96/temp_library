require('../connection')
const mongoose = require('mongoose')
const { create, drop, models, data } = require('./index')
const { Admins } = models
const { admins } = data

const init = async () => {
  try {
    const adm = await create(admins, Admins)
    console.log(adm.length, 'Admins created!')
    return Promise.resolve()
  } catch (e) {
    return Promise.reject(e)
  }
}

mongoose.connection.once('open', async () => {
  try {
    await init()
  } catch (e) {
    console.log(e)
  } finally {
    mongoose.disconnect()
  }
})
