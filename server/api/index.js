const requireDir = require('require-dir')
const express = require('express')
const router = express.Router()
const api = requireDir('./', { recurse: true })
const { Data } = api


router.get('/data', Data.get)
router.post('/data/:id', Data.update)
router.put('/data/:id', Data.create)
// router.delete('/organizations/:id'), Data.remove)''
/* ========== */

module.exports = router

router.get('/routes', (req, res) => {
  try {
    const routes = router.stack.map(layer => {
      const method = Object.keys(layer.route.methods)[0] || '???'
      const path = '/api' + layer.route.path
      const params = layer.keys.map(param => param.name)
      return { path, method, params }
    })
    res.send(JSON.stringify(routes, null, 2))
  } catch (error) {
    res.status(500).send(error)
  }
})
