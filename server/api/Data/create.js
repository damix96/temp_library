const { Data } = require('../../models')
// const { checkFields } = require('damvalidator')

const types = {}

module.exports = async (req, res) => {
  try {
    const data = await new Data(req.body.data).save()
    res.status(200).send({ data })
  } catch (err) {
    console.log(err)
    return res.status(500).send({ msg: 'Произошла ошибка', err: err.message })
  }
}
