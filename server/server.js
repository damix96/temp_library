const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const helmet = require('helmet')
const device = require('ip-device-parser')
const api = require('./api/index')
const port = 8765

const app = express()

app.use(helmet())
app.use(cors())
app.use(device())
app.use(morgan('dev'))
app.use(bodyParser.json({limit: '2mb'}))
app.use(bodyParser.urlencoded({limit: '2mb', extended: true}))

app.use(express.static('static/'))

app.use('/api', api)

app.listen(port, () => {
  console.log('Server running on ' + port)
})

module.exports = app
