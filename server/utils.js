const requireDir = require('require-dir')
const utils = requireDir('../Utils', { recurse: true })

module.exports = utils
